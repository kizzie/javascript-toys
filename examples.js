

var fruits = ["Apple", "Pear", "Banana"];

console.log(fruits[0]);
console.log(fruits.length)

function doSomething(num1, num2) {
    console.log('in the function');
    var result = num1 + num2
    return "result is: " + result
    
    function somethingElse() {
        console.log('oh hai');
    }
}

console.log(doSomething(1,2));

console.log(typeof(doSomething))

var student = new Object()

student['name'] = "fred";

console.log(student.name)
console.log(student['name'])

var student2 = {
    name : "Annie Apple",
    age : 1,
    incrementAge : function() {
        age ++
    },
    toString : function() {
        return this.name + ": " + this.age
    }
}

console.log(student2.age)

console.log(student2.toString())
//somethingElse()


var something = [
   {id: 1234, name: "Fred", email: "fred@business.com"},
   {id: 1235, name: "Wilma", email: "wilma@business.com"},
   {id: 1236, name: "Pebbles", email: "pebbles@business.com"}
];


for (var i=0; i<something.length; ++i)
   for (var key in something[i])
      console.log(key+": "+something[i][key]);

function kat() {
    x = 10;
}

kat();
console.log(x);

